console.log("Welcome to the World of Pokemon!")

let trainer = {

	name: "Ash Ketchum",
	age: "10",
	pokemon: ["Pikachu","Greninja","Charizard","Lucario"],
	friends: {
			Hoenn: ["May","Max"],
			Kanto: ["Brock","Misty"],
	},
	talk: function(){
		console.log("Pikachu, I choose you!");
	}
};
console.log(trainer);

console.log("Result of Dot Notation:");
console.log(trainer.name);
console.log("Result of Square Bracket Notation:");
console.log(trainer.pokemon);

trainer.talk();

function Pokemon(name,level){

	this.name = name;
	this.level = level;
	this.health = level*3;
	this.attack = level*1.5;
	this.tackle = function(thisPokemon,targetPokemon){
		console.log(thisPokemon.name + " tackled " + targetPokemon.name+" with Volt Tackle!");
	}
	this.faint = function(){
		console.log("Dialga has fainted!");
	}
};

let pokemon1 = new Pokemon("Geodude", 23);
let pokemon2 = new Pokemon("Dialga", 50);
let pokemon3 = new Pokemon("Pikachu", 50);
let pokemon4 = new Pokemon("Hariyama", 30);

console.log(pokemon1);
console.log(pokemon2);
console.log(pokemon3);

pokemon1.tackle(pokemon3,pokemon2);

pokemon2.health = 0;
console.log("Dialga's health is now " + pokemon2.health+"!");
pokemon1.faint();

