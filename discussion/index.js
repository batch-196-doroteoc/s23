console.log("Hello, World!");

//JS Objects
//JS Objects is a way to define a real world object with its own characteristics
//It is also a way to organize data with context through the use of key-value pairs

/*
	loyal,
	huggable,
	4 legs,
	furry,
	different colors,
	hyperactive,
	tail,
	breed,

*/


//Create a list, use an array
/*let dog = ["loyal", 4, "tail", "Husky"];*/
//Describe something with characteristics, use objects

let dog = {
	breed: "Husky",
	color: "White",
	legs: 4,
	isHuggable: true,
	isLoyal: true
};


let videoGame = {
	title: "Wild Rift",
	publisher: "Riot Games",
	year: "2020",
	price: 100,//"Free with in-game purchases",
	isAvailable: true
};
console.log(videoGame);

//If [] are useed to create arrays. what did you use to create your object?
//[] array literals
//{} object literals

//each line is a property
//objects are composed of key-value pairs. Key provide label to your values
//key: value
//each key-value pair together are called properties

//accesing array items = arrName[index]
//acessing Object properties = objectName.propertyName

console.log(videoGame.title);
//What if I want to access the publisher?

console.log(videoGame.publisher);

//Can we also update the properties of our object?

videoGame.price = 200;
console.log(videoGame);

videoGame.title = "Final Fantasy X";
videoGame.publisher = "Square Enix";
videoGame.year = "2001";
console.log(videoGame);

//objects can not only have primitive values like strngs, numbers or boolean
//it can also contain objects and arrays

let course = {

	title: "Philosophy 101",
	description: "Learn the values of life",
	price: 5000,
	isActive: true,
	instructors: ["Mr. Johnson", "Mrs. Smith", "Mr. Francis"]
};

console.log(course);
//We can access the instructors array...
console.log(course.instructors);
//We can access Mrs. Smith in the instructors array...
console.log(course.instructors[1]);
//Can we use the instructor array's array methods?
course.instructors.pop();
console.log(course);
//objectName.propertyNameArr.method();

course.instructors.push("Mr. McGee");
let isInstructor = course.instructors.includes("Mr. Johnson");

console.log(course.instructors);
console.log(isInstructor);

//Create a function to be able to add new instructors to our object

// function addNewInstructor(instructor){
// 	let isFound = course.instructors.includes(instructor);
// 	if(isFound===true){
// 		alert("Instructor already added");
// 	}else{
// 		course.instructors.push(instructor);
// 		alert("Thank you. Instructor added");
// 	}
// };


console.log(course);

function addNewInstructor(instructor){
	let isFound = course.instructors.includes(instructor);
	if(isFound===true){
		console.log("Instructor already added.");
	}else{
		course.instructors.push(instructor);
		console.log("Thank you. Instructor added.");
	}
};

addNewInstructor("Mr. Calinao");
addNewInstructor("Ms. Garcia");
addNewInstructor("Mr. Calinao");

console.log(course);

let instructor = {};
console.log(instructor);

//if you assign a value to a property that does not yet exist
//you are able to add a new property in the obj
instructor.name ="James Johnson";
console.log(instructor);

instructor.job = "Instructor";
instructor.age = 56;
instructor.gender = "male";
instructor.department ="Humanities";
instructor.currentSalary =50000;
instructor.subjectsTaught = ["Philosophy", "Humanities", "Logic"];

console.log(instructor);

instructor.address = {

	street: "#1 Maginhawa St.",
	city: "Quezon City",
	country: "Philippines"
}

console.log(instructor);
console.log(instructor.address.street);

//create objects using a constructor function

//create a reusable function to create objects whose structure and keys are the same. Think of creating a function that serves a blueprint for an object

function Superhero(name,superpower,powerlevel){

	//this keyword when added in a constructor function refers to the object that will be made by the function.
	/*
		{
			name: <valueOfParameterName>
			superpower: <valueOfParameterSuperpower>
			powerLevel: <valueOfParamterPowerLevel>

		}

	*/
	this.name = name;
	this.superpower = superpower;
	this.powerlevel = powerlevel;
};

//create an object out of our superhoro constructor function
//new keyword is added to allow us to create a new object out of our function.

let superhero1 = new Superhero("Saitama","One Punch", 30000);
console.log(superhero1);

//MA...
function Laptop(name,brand,price){

	this.name = name;
	this.brand = brand;
	this.price = price;
};

let laptop1 = new Laptop("ASUS ROG Zephyrus G14 GA401","ASUS", 41000);
let laptop2 = new Laptop("HP Spectre x360", "HP", 135290);

console.log(laptop1);
console.log(laptop2);

//Object Methods are functions that are associated with an object
//A function is a property of an objct that function belongs to the object
//Methods are tasks that an object can perform or do

//arrayName.method()

let person = {
	name: "Slim Shady",
	talk: function(){ //no need for para

		//methods are functions associated as a property of an object
		//They are anonymous functions we can invoke using the property of the object
		//this refers to the object where the method is associated
		console.log(this);
		console.log("Hi! My name is, What? My name is who? " + this.name);

	}
}

let person2 = {
	name: "Dr. Dre",
	greet: function(friend){ //nagpapasa ng data

			//greet() method should be able to receive a single object
			// console.log(friend);
			console.log("Good day," + friend.name);
	}
}


person.talk();
person2.greet(person);

//Create a constructor with a built-in method

function Dog(name,breed){

	/*
		{
			name: <valueParameterName>
			breed: <valueParameterBreed>

		}
	*/
	this.name = name;
	this.breed = breed;
	this.greet = function(friend){
		console.log("Bark! bark, " + friend.name);
	}

}

let dog1 = new Dog("Rocky","Bulldog");
console.log(dog1);
dog1.greet(person);
dog1.greet(person2);

let dog2 = new Dog("Blackie","Rottweiler");
console.log(dog2);

dog2.greet(dog1);